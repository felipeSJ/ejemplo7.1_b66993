package cr.ac.ucr.ecci.cql.mipruebas;

import android.content.Context;

import androidx.test.InstrumentationRegistry;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleInstrumentedTestCxt {

    @Test
    public void testContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("cr.ac.ucr.ecci.cql.mipruebas", appContext.getPackageName());
    }

}
